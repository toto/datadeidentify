# Author(s): Ermal Toto and ChatGPT 4.0
# 03/2024

import csv
import hashlib
import argparse

def hash_id_field(csv_path, output_path, id_field, remove_fields, unique_key):
    """
    Hashes a specified 'id_field' of a CSV file, optionally removes specified fields,
    and saves the results to a new CSV file, including a unique key in the hash process.

    Parameters:
    - csv_path: Path to the input CSV file.
    - output_path: Path to the output CSV file where results will be saved.
    - id_field: The name of the field to be hashed.
    - remove_fields: A list of fields to remove from the output.
    - unique_key: A unique key or token to combine with each ID before hashing.
    """
    with open(csv_path, mode='r', newline='', encoding='utf-8') as infile, \
         open(output_path, mode='w', newline='', encoding='utf-8') as outfile:

        reader = csv.DictReader(infile)
        original_fieldnames = reader.fieldnames
        
        # Ensure the id_field is in the CSV.
        if id_field not in original_fieldnames:
            raise ValueError(f"The CSV file must contain the '{id_field}' field.")

        # Prepare fieldnames for the output, removing specified fields.
        fieldnames = [f for f in original_fieldnames if f not in remove_fields]

        writer = csv.DictWriter(outfile, fieldnames=fieldnames)
        writer.writeheader()

        for row in reader:
            if id_field in row:
                # Combine the unique key with the ID before hashing.
                combined_id = unique_key + row[id_field]
                hashed_id = hashlib.sha256(combined_id.encode('utf-8')).hexdigest()
                row[id_field] = hashed_id

            # Write row without the removed fields.
            writer.writerow({field: row[field] for field in fieldnames})

def main():
    parser = argparse.ArgumentParser(description="Hash a specified field of a CSV file and optionally remove specified fields.")
    parser.add_argument("input_csv", help="Path to the input CSV file.")
    parser.add_argument("output_csv", help="Path to the output CSV file where results will be saved.")
    parser.add_argument("--id-field", required=True, help="The name of the field to hash.")
    parser.add_argument("--remove-fields", nargs='*', default=[], help="A list of fields to remove from the output CSV.")
    parser.add_argument("--unique-key", required=True, help="A unique key or token to add to each ID before hashing.")

    args = parser.parse_args()

    hash_id_field(args.input_csv, args.output_csv, args.id_field, args.remove_fields, args.unique_key)

if __name__ == "__main__":
    main()
